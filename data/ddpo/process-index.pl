#!/usr/bin/perl
#  Copyright (C) 2002 Igor Genibel                                         
#                                                                          
#  This program is free software; you can redistribute it and/or           
#  modify it under the terms of the GNU General Public License             
#  as published by the Free Software Foundation; either version 2          
#  of the License, or (at your option) any later version.                  
#                                                                          
#  This program is distributed in the hope that it will be useful,         
#  but WITHOUT ANY WARRANTY; without even the implied warranty of          
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the           
#  GNU General Public License for more details.                            
#
#  This code is currently maintained and debugged by Igor Genibel, any     
#  questions or comments regarding this code should be directed to:        
#  - igor@genibel.org                                                      

#  This code is greatly inspired from Raphael Hertzog's script for PTS
#  - Igor Genibel - http://genibel.org/                                    


use strict;
my %stats;
my %stats_real;
my %sources;
my %merge_list;
my %seen;

open SOURCE,"<$ARGV[1]" 
	or die "Unable to open sources file";

while(defined($_=<SOURCE>)) {
	$_=~s/\+/_/g;
	my ($b, $s) = m/^(\S+)\s+\S+\s+(\S+)$/;
	$sources{$b} = $s;
}

open BUGS,"<$ARGV[0]"
	or die "Unable to open bugs file";

while (defined($_=<BUGS>)) {
    $_=~s/\+/_/g;
    chomp;
    my ($pl, $b, $st, $severity, $tags) = m/^(\S+[,\S+]*) (\d+) \d+ (\S+) \[.*\] (\S+) (.*)$/;
    foreach my $p (split ',', $pl) {
	    my $nb_merge;
	    if (defined($p))
	    {
		next if ($st =~ /done/);
		$sources{$p} = $p if(!defined($sources{$p}));
		$b =~ m/\d+(\d{2})$/;
		open STATUS,"</srv/bugs.debian.org/spool/db-h/$1/$b.status";
		my $save = $/;
		$/ = undef;
		$nb_merge = 0;
		my @lines = split /\n/, <STATUS>;
		
		my @merge_list = split / /, $lines[8];
		# Flag the bug as a merged one
		foreach my $e (@merge_list)
		{
			$seen{$e} = 1;
		}
		# If bug number is flaged, don't count it
		if ($seen{$b} == 1 ){
			$nb_merge = 1;
		}
		#print "Package: $p, Bug Number: $b, nb_merge: $nb_merge\n";
		close STATUS;
		$/ = $save;
		my %tags = map { $_ => 1 } split ' ', $tags;
		if ($tags{pending} or $tags{fixed} or $severity eq 'fixed') {
		    $stats{$sources{$p}}{"fixed"}++;
		    $stats_real{$sources{$p}}{"fixed"}++;
		    $stats{$sources{$p}}{"fixed"}-=$nb_merge;
		} else {
		    $stats{$sources{$p}}{$severity}++;
		    $stats_real{$sources{$p}}{$severity}++;
		    $stats{$sources{$p}}{$severity}-=$nb_merge;
		}   
	    } else {
		print "Line badly formatted: $_\n";
	    }
	}
}

foreach (keys %stats) {
    my $grave = 0 + $stats{$_}{'critical'} + $stats{$_}{'grave'} + $stats{$_}{'serious'};
    my $grave_real = 0 + $stats_real{$_}{'critical'} + $stats_real{$_}{'grave'} + $stats_real{$_}{'serious'};
    my $normal = 0 + $stats{$_}{'normal'} + $stats{$_}{'important'};
    my $normal_real = 0 + $stats_real{$_}{'normal'} + $stats_real{$_}{'important'};
    my $wishlist = 0 + $stats{$_}{'wishlist'} + $stats{$_}{'minor'};
    my $wishlist_real = 0 + $stats_real{$_}{'wishlist'} + $stats_real{$_}{'minor'};
    my $fixed = 0 + $stats{$_}{'fixed'};
    my $fixed_real = 0 + $stats_real{$_}{'fixed'};
    print "$_\:$grave($grave_real) $normal($normal_real) $wishlist($wishlist_real) $fixed($fixed_real)\n";
}

