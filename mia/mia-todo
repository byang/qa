#!/usr/bin/python

# Look for specific tasks to be done
# Copyright (C) 2001, 2002, 2003  Martin Michlmayr <tbm@cyrius.com>
# Copyright (C) 2006  Jeroen van Wolffelaar <jeroen@wolffelaar.nl>

# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.

# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.

# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA


import re, string, sys, time
import apt_pkg, status

do_all = False
last_response = False

def help(target):
    target.write("""Usage: mia-todo [OPTION]... <mode>
Search for entries in the MIA database in need for a specific action

Mode can be 'needs-orphan', 'needs-{nice,prod,last-warning,wat}' or
'inject-{role,needs-wat}'

Options:
  -t, --templates           Write out templates to use

""")

def parse_args():
    global mode
    global Cnf, Options

    apt_pkg.init()
    Cnf = apt_pkg.Configuration()
    apt_pkg.read_config_file_isc(Cnf, status.config)

    Arguments = [("h", "help", "Todo::Options::Help"),
                 ("t", "templates", "Todo::Options::Templates"),
                 ]
    for i in ["Help", "Templates"]:
        if not Cnf.exists("Todo::Options::%s" % i):
            Cnf["Todo::Options::%s" % i] = ""

    mode = apt_pkg.parse_commandline(Cnf, Arguments, sys.argv)
    Options = Cnf.subtree("Todo::Options")

def ask():
    global last_response, do_all
    if do_all:
        return last_response

    yn = "Yes/*No*"
    if last_response:
        yn = "*Yes*/No"
    while 1:
        print "Mark in DB? ["+yn+"/Always/neVer/Quit]?"
        response = raw_input()
        if response == 'a':
            last_response = True
            do_all = True
            return True
        elif response == 'v':
            last_response = False
            do_all = True
            return False
        elif response == 'y':
            last_response = True
            return True
        elif response == 'n':
            last_response = False
            return False
        elif response == 'q':
            # Should print stats anyway... hm
            sys.exit(0)
        elif response == '':
            return last_response
        else:
            continue


def do_needs_mail(what):
    count = 0
    for maintainer, data in status.carnivore.iteritems():
        entry = status.get(maintainer)
        st = entry.getStatus()
        if st['status'] == what:

            if what == 'needs-wat':
                if st.has_key('suspend'): # and st['suspend'] == 'npa':
                    continue
                entry.prettyPrint()
            else:
                if st.has_key('suspend') or not entry.ce.package:
                    continue
                entry.prettyPrint()

            count = count+1
    print "%i maintainers in possible need of %s\n" % (count, what)

def do_inject_role():
    count = 0
    done = 0
    list_re = re.compile(r"lists\.", re.I)
    for maintainer, data in status.carnivore.iteritems():
        entry = status.get(maintainer)

        role = False
        for email in entry.ce.email:
            if list_re.search(email):
                role = True
        if role:
            if entry.statusIsAtLeast('role'): continue
            entry.prettyPrint()
            if ask():
                status.write_status(entry.mia_path_id, time.time(),
                    "role; Added with mia-todo inject-role", False)
                print 'Injected'
                done = done + 1
            else:
                print "Skipped"
            count = count+1
    print "%i new entries are now marked role (%i total)\n" % (done, count)

def do_inject_needs_wat():
    count = 0
    done = 0
    for maintainer, data in status.carnivore.iteritems():
        entry = status.get(maintainer)
        if entry.ce.keyring['ldap'] and not entry.ce.package:
            if entry.statusIsAtLeast('needs-wat'): continue
            entry.prettyPrint()
            if ask():
                reason = raw_input('Reason for WAT status: ');
                status.write_status(entry.mia_path_id, time.time(),
                    "needs-wat; "+reason, False)
                print 'Injected'
                done = done + 1
            else:
                print "Skipped"
            count = count+1
    print "%i new entries are now marked needs-wat (%i total)\n" % (done, count)

def do_needs_orphan():
    packages = {}
    for maintainer, data in status.carnivore.iteritems():
        entry = status.get(maintainer)
        mia = 0
        if entry.statusIsAtLeast('mia'):
            mia = 1
        if entry.statusIsAtLeast('role'):
            continue
        st = entry.getStatus()
        if st.has_key('suspend'):
            if st['suspend'] == 'ok' or st['suspend'] == 'willfix':
                continue
        for package in entry.ce.package:
            if not packages.has_key(package):
                packages[package] = ([], [])
            packages[package][mia].append(entry.ce)
    for package, maintainers in packages.iteritems():
        if not maintainers[0]:
            print "%s (%s)" \
                % (package, maintainers[1][0])

def main():
    global Options, mode

    parse_args()
    if Options["Help"]:
        help(sys.stdout)
        sys.exit(0)

    if len(mode) != 1:
        help(sys.stderr)
        sys.stderr.write("No mode specified!\n")
        sys.exit(1)

    mode = mode[0]
    if mode == 'needs-nice':
        do_needs_mail('busy')
    elif mode == 'needs-prod':
        do_needs_mail('inactive')
    elif mode == 'needs-last-warning':
        do_needs_mail('unresponsive')
    elif mode == 'needs-wat':
        do_needs_mail('needs-wat')
    elif mode == 'inject-needs-wat':
        do_inject_needs_wat()
    elif mode == 'inject-role':
        do_inject_role()
    elif mode == 'needs-orphan':
        do_needs_orphan()
    elif mode == 'needs-keyring-attention':
        # return those DD's that have something awkard regarding LDAP/keyring
        pass

    elif mode == "misc":
        # Used to be printed directly from carnivore, needs to go somewhere
        if len(ldap) > 1:
            warnings.append("Multiple LDAP entries")
        if keyring['emeritus'] and keyring['keyring']:
            warnings.append("Both emeritus and active")
        if keyring['ldap'] != keyring['keyring']:
            warnings.append("Ldap doesn't match keys in keyring")
        if keyring['emeritus'] and package:
            mia.append('emeritus-with-package')
        if not keyring['emeritus'] and not keyring['keyring'] and \
            keyring['removed'] and package:
            mia.append('removed-with-package')

main()


# vim: ts=4:expandtab:shiftwidth=4:
